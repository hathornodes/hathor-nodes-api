"""Test cases for IL Calculator using a mixture of join/exit transactions.."""
from datetime import datetime
import json

import unittest

from flaskr.helper import load_config
from flaskr.impermanent_loss_calculator import ImpermanentLossCalculator

class TestImpermanentLossCalculator(unittest.TestCase):
    def test_simple_il(self):
        """Test results against a set of pool join transactions."""
        address = 'osmo1ppmqpw4gjaat0tx6h597zdep7c8ezwgnegk0xf'
        calculator = ImpermanentLossCalculator(
            address,
            rpc='https://osmosis-1--rpc--archive.datahub.figment.io/apikey/5081e65fe6a617911d2e2e16d3947d7e/',
            lcd='https://osmosis-1--lcd--archive.datahub.figment.io/apikey/5081e65fe6a617911d2e2e16d3947d7e/')

        with open('tests/data/test_il_calculator.json', 'r') as read_content:
            expected = json.load(read_content)
        actual = calculator.calc_impermanent_loss()
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
