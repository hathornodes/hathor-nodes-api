"""Test cases for tax calculator using various blockchains & LP rewards."""
from datetime import datetime
import json

import unittest

from flaskr.helper import load_config
from flaskr.taxes import IncomeCalculator

class TestTaxes(unittest.TestCase):
    def test_lp_taxes(self):
        """Test results against a set of LP reward transactions."""
        address = 'osmo1ppmqpw4gjaat0tx6h597zdep7c8ezwgnegk0xf'
        config = load_config('osmo') 
        calculator = IncomeCalculator(
            address=address, token='OSMO', lcd=config['lcd_endpoint'],
            date_start=datetime.date(datetime(2021, 1, 1)),
            date_end=datetime.date(datetime(2021, 12, 31))
        )
        with open('tests/data/test_lp_taxes.json', 'r') as read_content:
            expected = json.load(read_content)
        actual = json.loads(calculator.get_taxable_income())
        self.assertEqual(actual, expected)

    def test_staking_taxes(self):
        """Test results against a set of OSMO staking transactions."""
        address = 'osmo1qx6kgrla69wmz90tn379p4kaux5prdkuzly2tw'
        config = load_config('osmo') 
        calculator = IncomeCalculator(
            address=address, token='OSMO', lcd=config['lcd_endpoint'],
            date_start=datetime.date(datetime(2021, 1, 1)),
            date_end=datetime.date(datetime(2021, 12, 31))
        )
        with open('tests/data/test_staking_taxes.json', 'r') as read_content:
            expected = json.load(read_content)
        actual = json.loads(calculator.get_taxable_income())
        self.assertEqual(actual, expected)

    def test_mix_taxes(self):
        """Test results against a mix of OSMO staking & LP transactions."""
        address = 'osmo1djv30wag0mth94tw6e3qwltl6swhzcwfyg3a4c'
        config = load_config('osmo') 
        calculator = IncomeCalculator(
            address=address, token='OSMO', lcd=config['lcd_endpoint'],
            date_start=datetime.date(datetime(2021, 1, 1)),
            date_end=datetime.date(datetime(2021, 12, 31))
        )
        with open('tests/data/test_mix_taxes.json', 'r') as read_content:
            expected = json.load(read_content)
        actual = json.loads(calculator.get_taxable_income())
        self.assertEqual(actual, expected)

    def test_non_osmo_staking_taxes(self):
        """Test results against a staking transactions outside of Osmosis."""
        address = 'bitsong1t26ag4sn8075dk27d8zp6dh30prnqzymeh9syq'
        config = load_config('btsg')
        calculator = IncomeCalculator(
            address=address, token='BTSG', lcd=config['lcd_endpoint'],
            date_start=datetime.date(datetime(2021, 1, 1)),
            date_end=datetime.date(datetime(2021, 12, 31))
        )
        with open('tests/data/test_non_osmo_staking_taxes.json', 'r') as read_content:
            expected = json.load(read_content)
        actual = json.loads(calculator.get_taxable_income())
        self.assertEqual(actual, expected)

if __name__ == '__main__':
    unittest.main()
