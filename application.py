from flask import Flask, jsonify, request, render_template, redirect, \
        url_for, make_response, send_from_directory, session, abort, Response
from flask_caching import Cache
from flaskr.event_streams import tax_event, impermanent_loss_event
from flaskr.aws_client import get_secret_value

from datetime import datetime
import json
import requests
import re

import pandas as pd
from scipy.stats import kurtosis
from sqlalchemy import create_engine
from sqlalchemy.pool import QueuePool

application = app = Flask(__name__)

# Application variables
app.config['DEBUG'] = True
app.config['SECRET_KEY'] = 'test secret key scheme testing'
app.config["JSON_SORT_KEYS"] = False
app.config['CACHE_TYPE'] = 'SimpleCache'
app.config['CACHE_DEFAULT_TIMEOUT'] = 300

# DB Connection Pools
db_creds = get_secret_value('hathor_nodes_db')
conn_string = 'mysql+pymysql://{user}:{password}@{host}/{db}'.format(
    user=db_creds['username'],
    password=db_creds['password'],
    host=db_creds['host'],
    db='osmosis')
cnx_pool = create_engine(conn_string, poolclass=QueuePool)

# Caching
cache = Cache(app)

@app.route('/', methods=['GET'])
def home():
    return render_template('index.html')

@app.route('/impermanent_loss', methods=['GET'])
def calc_impermanent_loss():
    address = request.args.get("address", "")
    response = Response(
        impermanent_loss_event(address), mimetype="text/event-stream")
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['X-Accel-Buffering'] = 'no'
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Connection'] = 'keep-alive'
    return response

@app.route('/income', methods=['GET'])
def get_taxable_transactions():
    address = request.args.get("address", "")
    start_date = request.args.get("startDate", None)
    end_date = request.args.get("endDate", None)

    if start_date and end_date:
        start_date = datetime.strptime(start_date, '%Y-%m-%d').date()
        end_date = datetime.strptime(end_date, '%Y-%m-%d').date()
    else:
        start_date = None
        end_date = None

    response = Response(tax_event(address, start_date, end_date), mimetype="text/event-stream")
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['X-Accel-Buffering'] = 'no'
    response.headers['Cache-Control'] = 'no-cache'
    response.headers['Connection'] = 'keep-alive'
    return response

@app.route('/validator_voting_history')
@cache.cached(timeout=60)
def get_validator_voting_history():
    try:
        conn = cnx_pool.connect()

        votes = conn.execute("""
            SELECT
                v.moniker,
                vv.proposal_id,
                CASE
                    WHEN vv.vote_option = 0 OR vv.vote_option IS NULL AND v.unbonding_time >= p.voting_end_time THEN 'Inactive'
                    WHEN vv.vote_option = 0 AND vv.vote_option IS NULL AND v.unbonding_time < p.voting_start_time THEN 'Missed'
                    WHEN vv.vote_option = 1 THEN 'Yes'
                    WHEN vv.vote_option = 2 THEN 'Abstain'
                    WHEN vv.vote_option = 3 THEN 'No'
                    WHEN vv.vote_option = 4 THEN 'No with Veto'
                ELSE NULL END AS vote
            FROM
                _validators v
            LEFT JOIN
                `_validator_votes` vv ON
                v.operator_address = vv.operator_address
            INNER JOIN proposals p ON
                p.id = vv.proposal_id;
        """)

        votes = pd.DataFrame(
            votes, columns=['moniker', 'Proposal Id', 'short_name'])

        votes_wide = pd.pivot_table(
            votes, values='short_name',
            index=['Proposal Id'], columns=['moniker'], aggfunc=max)
        votes_wide.reset_index(inplace=True)
        results = votes_wide.to_json(orient='records')
        results = json.loads(results)
        response = jsonify(results)
    except Exception as err:
        print(err)
        response = jsonify(
            {"message": "Failed to read validator voting history"})
 
    conn.close()
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response


@app.route('/validator_voting_statistics')
@cache.cached(timeout=60)
def get_validator_voting_statistics():
    conn = cnx_pool.connect()

    try:

        conn.execute("""
            DROP TABLE IF EXISTS voting_stats;""")

        conn.execute("""
            CREATE TEMPORARY TABLE voting_stats
            SELECT
                v.moniker,
                v.update_time AS start_date,
                CASE WHEN v.status = 3 THEN v.tokens/1000000 ELSE 0 END AS voting_power,
                CASE
                    WHEN v.jailed = 1 THEN 'Jailed'
                    WHEN v.status = 3 THEN 'Active'
                    ELSE 'Inactive'
                END AS Status,
                SUM(CASE WHEN vote_option = 1 THEN 1 ELSE 0 END) AS Yes,
                SUM(CASE WHEN vote_option = 3 THEN 1 ELSE 0 END) AS `No`,
                SUM(CASE WHEN vote_option = 2 THEN 1 ELSE 0 END) AS `Abstain`,
                SUM(CASE WHEN vote_option = 4 THEN 1 ELSE 0 END) AS Veto,
                SUM(CASE WHEN (vote_option = 0 OR vote_option is NULL) AND v.update_time > p.voting_end_time THEN 1 ELSE 0 END) AS Inactive,
                SUM(CASE WHEN (vote_option = 0 OR vote_option is NULL) AND v.update_time <= p.voting_end_time THEN 1 ELSE 0 END) AS Missed
            FROM
                proposals p
            CROSS JOIN
                `_validators` v ON
                1 = 1
            LEFT JOIN
                `_validator_votes` votes ON
                votes.operator_address = v.operator_address
                AND p.id = votes.proposal_id
            WHERE
                p.status NOT IN (0, 2, 1, 5)
            GROUP BY
                v.operator_address;
        """)

        voting_stats = conn.execute("""
            SELECT
                *,
                ROUND(100*(1 - Missed/(Yes + `No` + Abstain + Veto + Missed)), 1) AS Participation,
                ROUND(100*(Abstain/(Yes + `No` + Abstain + Veto)), 1) AS `Abstention Rate`
            FROM
                voting_stats
            ORDER BY
                Participation DESC;
        """)

        result = [
            {
                'moniker': validator[0],
                'start_date': validator[1].strftime('%Y-%m-%d %H:%M:%S'),
                'voting_power': float(validator[2]) if validator[2] is not None else None,
                'status': validator[3],
                'yes': int(validator[4]),
                'no': int(validator[5]),
                'abstain': int(validator[6]),
                'veto': int(validator[7]),
                'inactive': int(validator[8]),
                'missed': int(validator[9]),
                'participation_rate': float(validator[10]) if validator[10] is not None else None,
                'abstention_rate': float(validator[11]) if validator[11] is not None else None
            }
            for validator in voting_stats
        ]
        response = jsonify(result)
    except Exception as err:
        print(err)
        response = jsonify(
            {"message": "Failed to read validator voting statistics"})

    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/resources/<blockchain>/governance_record', methods=['GET'])
@cache.cached(timeout=60)
def get_governance_record(blockchain):
    # TODO: Replace cnx_pool w/ cnx_pool[blockchain].connect()
    conn = cnx_pool.connect()
    governance_record = conn.execute("""
        SELECT
            p.id,
            CASE
                WHEN p.status = 2 THEN 'Voting Period'
                WHEN p.status = 3 THEN 'Proposal Passed'
                WHEN p.status = 4 THEN 'Proposal Rejected'
                WHEN p.status = 5 THEN 'Proposal Failed'
            END AS proposal_status,
            p.title,
            p.description,
            p.yes,
            p.`no`,
            p.no_with_veto,
            p.abstain,
            yes + `no` + no_with_veto + abstain AS total_votes,
            CASE
                WHEN vo.vote_option = 1 THEN 'Yes'
                WHEN vo.vote_option = 3 THEN 'No'
                WHEN vo.vote_option = 2 THEN 'Abstain'
                WHEN vo.vote_option = 4 THEN 'Veto'
                WHEN (vo.vote_option = 0
                OR vo.vote_option is NULL)
                AND v.update_time > p.voting_end_time THEN 'Inactive'
                WHEN vo.vote_option = 0
                OR vo.vote_option IS NULL THEN 'Missed Vote'
            END AS my_vote,
            CASE
                WHEN vo.vote_option = 0 OR vo.vote_option IS NULL THEN NULL
                ELSE gr.reason
            END AS my_reason
        FROM
            proposals p
        LEFT JOIN `_validator_votes` vv ON
            vv.proposal_id = p.id
        INNER JOIN `_validators` v ON
            v.operator_address = vv.operator_address
        INNER JOIN vote_options vo ON
            vo.vote_option = vv.vote_option
        INNER JOIN
            governance_record gr ON gr.proposal_id = p.id
        WHERE
            v.operator_address = 'osmovaloper1qx6kgrla69wmz90tn379p4kaux5prdkucgvfuf'
            AND p.status IN (3, 4, 5)
            AND p.voting_end_time <= NOW()
        ORDER BY
            id DESC;
    """) 

    result = [
        {
            'id': vote[0],
            'proposal_status': vote[1],
            'title': vote[2],
            'description': vote[3],
            'yes': vote[4],
            'no': vote[5],
            'no_with_veto': vote[6],
            'abstain': vote[7],
            'total_votes': vote[8],
            'my_vote': vote[9],
            'my_reason': vote[10]
        }
        for vote in governance_record
    ]

    try:
        response = jsonify(result)
    except Exception as err:
        print(err)
        response = jsonify({"message": "Failed to fetch governance record"})
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/resources/osmosis/sfs_data_pools')
@cache.cached(timeout=60)
def get_sfs_pool_data():
    """Get SFS data aggregated at the pool level."""
    conn = cnx_pool.connect()
    sfs_agg_pool = conn.execute("""
        SELECT
            epoch,
            denom,
            n_sfs_delegations,
            osmo_delegated,
            perc_pool_sfs
        FROM
            mv_sfs_pool_agg  
        ORDER BY
            denom,
            epoch DESC; 
    """)

    result = [
        {
            'epoch': pool[0],
            'pool_id': re.search('(?<=gamm/pool/)[0-9]*', pool[1]).group(0),
            'n_sfs_delegations': pool[2],
            'osmo_delegated': pool[3], 
            'perc_pool_sfs': pool[4]
        }
        for pool in sfs_agg_pool
    ]
    response = jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/resources/osmosis/sfs_data_nodes')
@cache.cached(timeout=60)
def get_sfs_node_data():
    """Get SFS data aggregated at node level."""
    conn = cnx_pool.connect()
    sfs_agg = conn.execute("""
        SELECT
            hv.epoch,
            v.status,
            hv.moniker,
    	    n_delegations AS n_sfs_delegations,
            POW(10, -6) * hv.tokens - IFNULL(sas.osmo_delegated, 0) AS osmo_staked,
            IFNULL(sas.osmo_delegated, 0) AS osmo_sf_staked,
            ROUND(100*IFNULL(sas.osmo_delegated, 0)/(POW(10, -6) * hv.tokens), 1) AS perc_stake_sfs
        FROM
            `_validators` v
        LEFT JOIN
            hist_validators hv ON
            v.operator_address = hv.operator_address
        LEFT JOIN
            mv_sfs_validator_agg sas ON
            hv.epoch = sas.epoch
            AND v.operator_address = sas.operator_address
        WHERE
            hv.epoch IS NOT NULL
            AND hv.status = 3
        ORDER BY
            hv.epoch DESC,
            perc_stake_sfs DESC;
    """)

    result = [
        {
            'epoch': node[0],
            'status': node[1],
            'moniker': node[2],
            'n_sfs_delegations': node[3],
            'osmo_staked': node[4],
            'osmo_sf_staked': node[5],
            'perc_stake_sfs': node[6]
        }
        for node in sfs_agg
    ]
    response = jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/resources/osmosis/current_pool_count', methods=['GET'])
@cache.cached(timeout=60)
def get_current_pool_count():
    """Return the number of pools at the current epoch."""
    conn = cnx_pool.connect()
    # TODO: Verify if limit speeds up this query.
    # In theory it meets the "4 principals" since we're only grouping by the pk
    pool_count = conn.execute("""
        SELECT
            hp.epoch,
            COUNT(DISTINCT id) AS n_pools
        FROM
            hist_pools hp
        GROUP BY
            hp.epoch
        ORDER BY
            hp.epoch DESC
        LIMIT 1;
    """)
    result = [count[1] for count in pool_count]
    response = jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/resources/osmosis/validator_kurtosis')
@cache.cached(timeout=60)
def get_hist_validator_kurtosis():
    """Return kurtosis at each epoch for all validators."""
    conn = cnx_pool.connect()
    delegations = conn.execute("""
        SELECT
            start_time,
            POW(10, -6) * tokens
        FROM
            hist_validators
        INNER JOIN
            epochs ON
            epochs.epoch = hist_validators.epoch
    """)
    delegations = pd.DataFrame(delegations, columns=["start_time", "kurt"])
    delegations = delegations.groupby("start_time").agg({"kurt": kurtosis})
    delegations = delegations.reset_index()

    result = delegations.to_dict('records')
    response = jsonify(result)
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

@app.route('/health')
def health():
    response = jsonify({ "health": "ok" })
    response.headers.add('Access-Control-Allow-Origin', '*')
    return response

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8000, threaded=True)
