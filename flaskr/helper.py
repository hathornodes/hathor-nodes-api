"""Helper functions for reward automation."""
import base64
import codecs
import json
import time
from datetime import datetime

from google.protobuf.json_format import MessageToDict
import pandas as pd
import requests

import flaskr.interface.cosmos.bank.v1beta1.query_pb2 as query_balance

def get_block_times(endpoint, height):
    """Get the UTC time of each block."""
    req = f'{endpoint}block?height={height}'
    res = requests.get(req).json()
    block_time = res["result"]["block"]["header"]["time"]

    # Reduce time to nearest millisecond for strptime
    block_time = datetime.strptime(block_time[:len(block_time)-4], '%Y-%m-%dT%H:%M:%S.%f')
    block_time = time.mktime(block_time.timetuple())
    return block_time


def get_token_price(token):
    """Get price of token over time."""
    # range can be set to 1y instead, faster but less accurate
    req = f'https://api-osmosis.imperator.co/tokens/v1/historical/{token}/chart?range=1y' # 365d'
    res = requests.get(req).json()
    prices = pd.DataFrame(res)
    return prices


def find_price_at_block(token, block_time):
    """Find price of token at given time."""
    prices = get_token_price(token)
    prices["time_comp"] = abs(prices["time"] - block_time)
    price = prices[prices["time_comp"] == prices.time_comp.min()]
    return price.close.values[0]


def calc_sequence(config, address):
    """Calculate sequence for transactions."""
    url = f'{config["lcd"]}cosmos/tx/v1beta1/txs?events=message.sender=\'{address}\''
    res = requests.get(url).json()
    try:
        sequence = res["txs"][-1]["auth_info"]["signer_infos"][0]["sequence"]
        sequence = int(sequence) + 1
    except IndexError:
        sequence = 0
    return sequence


def query_balance(address, denom, rpc_endpoint, height):
    """Query account balance of a specific denom coin."""
    proto = query_balance.MsgQueryBalanceRequest()
    proto.address = address
    proto.denom = denom
    hex_proto = codecs.encode(proto.SerializeToString(), 'hex')
    hex_proto = str(hex_proto, 'utf-8')
    req = {
        "jsonrpc": "2.0",
        "id":"1",
        "method":"abci_query",
        "params": {
            "height": str(height),
            "path": "/cosmos.bank.v1beta1.Query/Balance",
            "data": hex_proto
        }
    }
    req = json.dumps(req)
    response = requests.post(rpc_endpoint, req).text # .json()
    response = json.loads(response)
    response = response['result']['response']['value']

    response = base64.b64decode(response)
    balance = query_balance.MsgQueryBalanceResponse()
    balance.ParseFromString(response)
    balance = MessageToDict(balance)
    return balance['balance']['amount']


def get_hrp_config():
    return {
        "bitsong": {
            "rpc_endpoint": "https://rpc-bitsong.itastakers.com/",
            "lcd_endpoint": "https://lcd.explorebitsong.com/",
            "validator": "bitsongvaloper1t26ag4sn8075dk27d8zp6dh30prnqzymcnee5a",
            "path": "m/44'/639'/0'/0/0",
            "hrp": "bitsong",
            "chain_id": "bitsong-2b",
            "account_number": 5626,
            "sequence": 43,
            "gas": 500000,
            "fee": "7500",
            "denom": "ubtsg",
            "commission": True,
            "name": "btsg"
        },
        "osmo": {
            "rpc_endpoint": "http://142.132.157.153:26657/",
            "lcd_endpoint": "http://142.132.157.153:1317/",
            "validator": "osmovaloper1qx6kgrla69wmz90tn379p4kaux5prdkucgvfuf",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "osmo",
            "chain_id": "osmosis-1",
            "account_number": 143037,
            "sequence": 21,
            "gas": 500000,
            "fee": "0",
            "denom": "uosmo",
            "commission": True,
            "name": "osmo"
        },
        "lum": {
            "rpc_endpoint": "https://node0.mainnet.lum.network/rpc/",
            "lcd_endpoint": "https://node0.mainnet.lum.network/rest/",
            "validator": "lumvaloper1qx6kgrla69wmz90tn379p4kaux5prdkuqerqnv",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "lum",
            "chain_id": "lum-network-1",
            "account_number": 81618,
            "sequence": 21,
            "gas": 500000,
            "fee": "5000",
            "denom": "ulum",
            "commission": True,
            "name": "lum"
        },
        "sent": {
            "rpc_endpoint": "https://rpc.sentinel.co/",
            "lcd_endpoint": "https://lcd.sentinel.co/",
            "validator": "sentvaloper163g6kygmh08mgp0kj4gkenl4hj6nsqzguk6rzf",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "sent",
            "chain_id": "sentinelhub-2",
            "account_number": 19144,
            "sequence": 7,
            "gas": 500000,
            "fee": "50000",
            "denom": "udvpn",
            "source_channel": "channel-2",
            "commission": False,
            "name": "dvpn"
        }
    }


def get_config():
    return {
        "btsg": {
            "rpc_endpoint": "https://rpc-bitsong.itastakers.com/",
            "lcd_endpoint": "https://lcd.explorebitsong.com/",
            "validator": "bitsongvaloper1t26ag4sn8075dk27d8zp6dh30prnqzymcnee5a",
            "path": "m/44'/639'/0'/0/0",
            "hrp": "bitsong",
            "chain_id": "bitsong-2b",
            "account_number": 5626,
            "sequence": 43,
            "gas": 500000,
            "fee": "7500",
            "denom": "ubtsg",
            "commission": True
        },
        "osmo": {
            "rpc_endpoint": "http://142.132.157.153:26657/",
            "lcd_endpoint": "http://142.132.157.153:1317/",
            "validator": "osmovaloper1qx6kgrla69wmz90tn379p4kaux5prdkucgvfuf",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "osmo",
            "chain_id": "osmosis-1",
            "account_number": 143037,
            "sequence": 21,
            "gas": 500000,
            "fee": "0",
            "denom": "uosmo",
            "commission": True
        },
        "lum": {
            "rpc_endpoint": "https://node0.mainnet.lum.network/rpc/",
            "lcd_endpoint": "https://node0.mainnet.lum.network/rest/",
            "validator": "lumvaloper1qx6kgrla69wmz90tn379p4kaux5prdkuqerqnv",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "lum",
            "chain_id": "lum-network-1",
            "account_number": 81618,
            "sequence": 21,
            "gas": 500000,
            "fee": "5000",
            "denom": "ulum",
            "commission": True
        },
        "dvpn": {
            "rpc_endpoint": "https://rpc.sentinel.co/",
            "lcd_endpoint": "https://lcd.sentinel.co/",
            "validator": "sentvaloper163g6kygmh08mgp0kj4gkenl4hj6nsqzguk6rzf",
            "path": "m/44'/118'/0'/0/0",
            "hrp": "sent",
            "chain_id": "sentinelhub-2",
            "account_number": 19144,
            "sequence": 7,
            "gas": 500000,
            "fee": "50000",
            "denom": "udvpn",
            "source_channel": "channel-2",
            "commission": False
        }
    }


def load_config(blockchain):
    """Load chain info for a given blockchain."""
    return get_config()[blockchain]


def load_hrp_config(address):
    hrp = ''
    hrp_config = get_hrp_config()
    for char in address:
        hrp += char
        if hrp in hrp_config:
            return hrp_config[hrp]

    return None
