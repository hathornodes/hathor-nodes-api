"""Calculate the impermanent loss/gain experienced by an Osmosis user."""
import base64
import codecs
import json
import time
from collections import Counter

import requests
from google.protobuf.json_format import MessageToDict

import flaskr.interface.osmosis.gamm.v1beta1.query_pb2 as query_pools

class ImpermanentLossCalculator():
    """Data Object for calculating value of a gamm."""
    def __init__(
        self,
        address: str,
        lcd: str,
        rpc: str
    ) -> None:
        self.address = address
        self.lcd = lcd
        self.rpc = rpc
        self.pools = requests.get(
            'https://api-osmosis.imperator.co/search/v1/pools').json()
        self.valid_tx_types = [
            '/osmosis.gamm.v1beta1.MsgJoinPool',
            '/osmosis.gamm.v1beta1.MsgExitPool',
            '/osmosis.gamm.v1beta1.MsgJoinSwapExternAmountIn',
            '/osmosis.gamm.v1beta1.MsgExitSwapExternAmountOut'
        ]

    def _get_pool_transactions(self) -> list:
        """Pull all pool txs from LCD client."""
        offset = 0
        limit = 100

        url = (f"{self.lcd}cosmos/tx/v1beta1/txs?events=message.sender="
               f"'{self.address}'&pagination.offset={offset}&"
               f"pagination.limit={limit}")

        res = requests.get(url).json()
        page_total = int(res['pagination']['total'])/limit
        pool_txs = []

        for idx, transaction in enumerate(res['txs']):
            for msg in transaction['body']['messages']:
                if msg['@type'] not in self.valid_tx_types:
                    continue

                pool_tx = msg.copy()
                pool_tx['height'] = res['tx_responses'][idx]['height']
                pool_txs.append(pool_tx)

        offset += limit
        while offset/limit <= int(page_total):
            url = (f"{self.lcd}cosmos/tx/v1beta1/txs?events=message.sender="
                   f"'{self.address}'&pagination.offset={offset}&"
                   f"pagination.limit={limit}")
            res = requests.get(url).json()

            for idx, transaction in enumerate(res['txs']):
                for msg in transaction['body']['messages']:
                    if msg['@type'] not in self.valid_tx_types:
                        continue

                    pool_tx = msg.copy()
                    pool_tx['height'] = res['tx_responses'][idx]['height']
                    pool_txs.append(pool_tx)
            offset += limit
        return pool_txs

    def _format_pool_transactions(self) -> list:
        """Collect the join and exit pool transactions."""
        raw_txs = self._get_pool_transactions()
        pool_txs = []
        for msg in raw_txs:
            if msg['@type'] == '/osmosis.gamm.v1beta1.MsgJoinPool':
                share_key = 'shareOutAmount'
                token_key = 'tokenInMaxs'
            elif msg['@type'] == '/osmosis.gamm.v1beta1.MsgExitPool':
                share_key = 'shareInAmount'
                token_key = 'tokenOutMins'
            elif msg['@type'] == '/osmosis.gamm.v1beta1.MsgJoinSwapExternAmountIn':
                share_key = 'shareOutMinAmount'
                token_key = 'tokenIn'
                msg[token_key] = [msg[token_key], {'amount': 0, 'denom': None}]

            pool_tx = {
                'height': msg['height'],
                '@type': msg['@type'],
                'pool_id': msg['poolId'],
                'gamms': int(msg[share_key]),
                'token_1': msg[token_key][0]['denom'],
                'token_1_amount': int(msg[token_key][0]['amount']),
                'token_2': msg[token_key][1]['denom'],
                'token_2_amount': int(msg[token_key][1]['amount'])
            }
            pool_txs.append(pool_tx)
        return pool_txs

    def _send_abci_query(
            self, request_msg, path: str, response_msg, height: int
    ) -> dict:
        """Encode and send pre-filled protobuf msg to RPC endpoint."""
        # Some queries have no data to pass.
        if request_msg:
            request_msg = codecs.encode(request_msg.SerializeToString(), 'hex')
            request_msg = str(request_msg, 'utf-8')
        req = {
            "jsonrpc": "2.0",
            "id": "1",
            "method": "abci_query",
            "params": {
                "height": str(height),
                "path": path,
                "data": request_msg
            }
        }
        req = json.dumps(req)
        response = requests.post(self.rpc, req).json()
        if 'result' not in response:
            print(response)
        response = response['result']['response']['value']
        response = base64.b64decode(response)
        result = response_msg()
        result.ParseFromString(response)
        result = MessageToDict(result)
        return result

    def get_token_price(self, pool_id: int, token: str, height: int) -> float:
        """Get price of token at given height w/ UST as a price reference."""

        # Calculate the price of Osmo in UST.
        msg = query_pools.QuerySpotPriceRequest()
        msg.poolId = 560 # Osmo/UST Pool
        msg.tokenInDenom = 'ibc/BE1BB42D4BE3C30D50B68D7C41DB4DFCE9678E8EF8C539F6E6A9345048894FCC'
        msg.tokenOutDenom = 'uosmo'
        msg.withSwapFee = False
        osmo_price = self._send_abci_query(
            msg, "/osmosis.gamm.v1beta1.Query/SpotPrice",
            query_pools.QuerySpotPriceResponse, height)
        osmo_price = float(osmo_price['spotPrice'])
        if token != 'uosmo':
            # Calculate the price of the token in Osmos
            pricing = query_pools.QuerySpotPriceRequest()
            pricing.poolId = pool_id
            pricing.tokenInDenom = 'uosmo'
            pricing.tokenOutDenom = token
            pricing.withSwapFee = False
            token_price = self._send_abci_query(
                pricing, "/osmosis.gamm.v1beta1.Query/SpotPrice",
                query_pools.QuerySpotPriceResponse, height
            )
            token_price = float(token_price['spotPrice']) * osmo_price
        else:
            token_price = osmo_price
        return token_price

    def _count_pool_tokens(self, pool_id: int, height: int) -> dict:
        """Identify number of tokens in a GAMM at a given height."""
        msg = query_pools.QueryPoolAssetsRequest()
        msg.poolId = pool_id
        token_count = self._send_abci_query(
            msg, "/osmosis.gamm.v1beta1.Query/PoolAssets",
            query_pools.QueryPoolAssetsResponse, height)
        return token_count['poolAssets']

    def _get_total_shares(self, pool_id: int, height:int) -> int:
        """Get the total number of GAMMs in a pool at a given height."""
        msg = query_pools.QueryTotalSharesRequest()
        msg.poolId = pool_id
        total_shares = self._send_abci_query(
            msg, "/osmosis.gamm.v1beta1.Query/TotalShares",
            query_pools.QueryTotalSharesResponse, height)
        return int(total_shares['totalShares']['amount'])

    def _calc_tokens_per_gamm(self, pool_id: int, height: int) -> dict:
        """Calculate the number of each token per gamm in a pool."""
        count = self._count_pool_tokens(pool_id, height)
        gamms = self._get_total_shares(pool_id, height)
        tokens_per_gamm = {
            coin['token']['denom']: int(coin['token']['amount'])/gamms
            for coin
            in count
        }
        tokens_per_gamm['height'] = int(height)
        return tokens_per_gamm

    def _price_gamm(self, pool_id: int, height: int) -> float:
        """Get the price of a gamm at the given height."""
        gamm_tokens = self._calc_tokens_per_gamm(pool_id, height)
        gamm_tokens.pop('height', None)
        gamm_value = 0
        
        for token in gamm_tokens:
            if 'uosmo' not in gamm_tokens:
                for pool, vals in self.pools.items():
                    if vals[0]['denom'] in [token, 'uosmo'] and vals[1]['denom'] in [token, 'uosmo']:
                        token_value_added = self.get_token_price(int(pool), token, height)
                        break
            else:
                token_value_added = self.get_token_price(pool_id, token, height)
            token_value_added *= float(gamm_tokens[token])/1000000
            gamm_value += token_value_added
        return gamm_value

    def _calc_impermanent_loss(self, pool_txs: list) -> tuple:
        """Calculate il for a list of lots and return remaining open lots."""
        url = f'{self.rpc}status?'
        curr_height = requests.get(url).json()
        curr_height = curr_height['result']['sync_info']['latest_block_height']

        # Iterate through transactions. If joining pool, price gamm at join.
        # If leaving pool, price gamm at leave.
        # Remove tokens gained from priced gamm from buy lots.
        # Difference is IL.
        token_pool_map = {}
        token_count = {}
        lots = []
        impermanent_losses = []

        for pool_tx in pool_txs:
            tx_type = pool_tx.get('@type', None)
            if tx_type  == '/osmosis.gamm.v1beta1.MsgJoinPool':
                lots.append(
                    {
                        'pool_id': pool_tx['pool_id'],
                        'amount': int(pool_tx['gamms']),
                        'token_1': pool_tx['token_1'],
                        'token_1_amount': int(pool_tx['token_1_amount']),
                        'token_2': pool_tx['token_2'],
                        'token_2_amount': int(pool_tx['token_2_amount'])
                    }
                )
            elif tx_type == '/osmosis.gamm.v1beta1.MsgExitPool':
                pre_pool = {
                    'token_1': None, 'token_2': None,
                    'token_1_amount': 0, 'token_2_amount': 0
                }
                for lot in lots:
                    if lot['pool_id'] != pool_tx['pool_id']:
                        continue
                    token_1 = pool_tx['token_1']
                    token_2 = pool_tx['token_2']
                    if not pre_pool['token_1']:
                        pre_pool['token_1'] = pool_tx['token_1']
                        pre_pool['token_2'] = pool_tx['token_2']
                    elif pre_pool['token_1'] == lot['token_1']:
                        token_1 = lot['token_1']
                        token_2 = lot['token_2']
                    else:
                        token_1 = lot['token_2']
                        token_2 = lot['token_1']

                    if lot['amount'] >= pool_tx['gamms']:
                        perc_lot_used = pool_tx['gamms']/lot['amount']
                        lot['amount'] -= pool_tx['gamms']
                        pre_pool['token_1_amount'] += perc_lot_used*lot['token_1_amount']
                        pre_pool['token_2_amount'] += perc_lot_used*lot['token_2_amount']
                    elif lot['amount'] <= pool_tx['gamms']:
                        perc_tx_used = lot['amount']/pool_tx['gamms']
                        pool_tx['gamms'] -= lot['amount']
                        pool_tx['token_1_amount'] *= perc_tx_used
                        pool_tx['token_2_amount'] *= perc_tx_used
                        pre_pool['token_1_amount'] += lot['token_1_amount']
                        pre_pool['token_2_amount'] += lot['token_2_amount']
                    break

                # Remove emptied lots and log IL
                lots = [lot for lot in lots if lot['amount'] != 0]
                impermanent_losses.append({
                    'token_1': pre_pool['token_1'],
                    'token_2': pre_pool['token_2'],
                    'token_1_il': pool_tx['token_1_amount'] - pre_pool['token_1_amount'],
                    'token_2_il': pool_tx['token_2_amount'] - pre_pool['token_2_amount']
                })

        lot_il = {}
        for il in impermanent_losses:
            if il['token_1'] in lot_il:
                lot_il[il['token_1']]['amount'] += il['token_1_il']
            else:
                lot_il[il['token_1']] = {}
                lot_il[il['token_1']]['amount'] = il['token_1_il']
            if il['token_2'] in lot_il:
                lot_il[il['token_2']]['amount'] += il['token_2_il']
            else:
                lot_il[il['token_2']] = {}
                lot_il[il['token_2']]['amount'] = il['token_2_il']
        return (lots, lot_il)

    def calc_impermanent_loss(self) -> dict:
        """Recursively calc IL until all pool txs are exhausted."""
        pool_txs = self._format_pool_transactions()
        total_il = {}
        while pool_txs:
            pool_txs, curr_il = self._calc_impermanent_loss(pool_txs)
            print(curr_il, flush=True)
            for token, il in curr_il.items():
                if token in total_il:
                    total_il[token] += il
                else:
                    total_il[token] = il

            # Create counter parts to remaining lot of buys to negate it.
            psuedo_txs = []
            for pool_tx in pool_txs:
                tx_type = pool_tx.get('@type', None)
                if tx_type == '/osmosis.gamm.v1beta1.MsgExitPool':
                    print("Critical Error 1!")
                elif tx_type == '/osmosis.gamm.v1beta1.MsgJoinPool':
                    psuedo_tx = pool_tx.copy()
                    psuedo_tx['@type'] = '/osmosis.gamm.v1beta1.MsgExitPool'
                    psuedo_txs.append(psuedo_tx)
            pool_tx = psuedo_txs

        # TODO price these tokens
        return total_il
