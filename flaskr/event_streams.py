import gevent
from gevent.pywsgi import WSGIServer
from gevent import monkey,  Greenlet
monkey.patch_all()
import json
from datetime import datetime

from flaskr.impermanent_loss_calculator import ImpermanentLossCalculator
from flaskr.taxes import IncomeCalculator
from flaskr.helper import load_hrp_config
from flaskr.pools import get_ibc_name_mapping

def tax_event(address,start_date, end_date):
    hrp_config = load_hrp_config(address)
    if not hrp_config:
        raise Exception("Failed to find address")

    ic = IncomeCalculator(address, hrp_config['name'], hrp_config['lcd_endpoint'], start_date, end_date)
    taxable_income_greenlet = Greenlet(ic.get_taxable_income)
    taxable_income_greenlet.start()
    while True:
        if taxable_income_greenlet.ready():
            print("Tax ready")
            result = json.loads(taxable_income_greenlet.get())
            total_income = 0
            tokens = {}
            for data in result:
                epoch = data['day']//1000
                data['date'] = datetime.fromtimestamp(epoch).strftime('%Y-%m-%d')

                data['amount'] = round(data['amount'], 6)
                data['price'] = f'{data["price"]:.2f}'

                if data['token'] not in tokens:
                    tokens[data['token']] = 0

                tokens[data['token']] += data['amount']

                data['income'] = round(data['income'], 2)
                total_income += data['income']
                data['income'] = f'{data["income"]:.2f}'

            aggregates = {
                'total_income': round(total_income, 2),
                'tokens': tokens
            }

            result = sorted(result, key=lambda data: (-data['day'], data['type'], data['token']))

            yield 'data: ' + json.dumps({"result": result, "aggregates": aggregates, "finished": True })+ '\n\n'
            break;
        yield 'data: ' + json.dumps({"finished": False})+ '\n\n'
        print("Processing Tax...")
        gevent.sleep(3)

    print("Finished processing Tax")


def impermanent_loss_event(address):
    print("Triggered impermanent loss event for address: {}".format(address))
    hrp_config = load_hrp_config(address)
    if not hrp_config:
        raise Exception("Failed to find address")

    il = ImpermanentLossCalculator(address, hrp_config['lcd_endpoint'], hrp_config['rpc_endpoint'])

    impermanent_loss_greenlet = Greenlet(il.calc_impermanent_loss)
    impermanent_loss_greenlet.start()
    while True:
        if impermanent_loss_greenlet.ready():
            print("Impermanent Loss ready")
            result = impermanent_loss_greenlet.get()
            ibc_name_mapping = get_ibc_name_mapping()
            token_held_value = []
            total_impermanent_loss = 0
            for token_name, token_value in result.items():
                token_amount = round(token_value['amount'] // 1000000, 2)
                price = round(ibc_name_mapping[token_name]['price'], 2)
                total = round(token_amount * price, 2)
                if token_name == 'uosmo' or 'ibc' in token_name:
                    name = token_name
                    if ibc_name_mapping and token_name in ibc_name_mapping:
                        name = ibc_name_mapping[token_name]

                    token_held_value.append({
                        'name': ibc_name_mapping[token_name]['symbol'],
                        'price': price,
                        'amount': token_amount,
                        'total': total
                    })
                total_impermanent_loss += total

            result['token_held_value'] = token_held_value
            result['impermanent_loss_value'] = round(total_impermanent_loss, 2)
            yield 'data: ' + json.dumps({"result": result, "finished": True })+ '\n\n'
            break;

        yield 'data: ' + json.dumps({"finished": False})+ '\n\n'
        print("Processing Impermanent Loss...")
        gevent.sleep(3)

    print("Finished processing Impermanent Loss")
