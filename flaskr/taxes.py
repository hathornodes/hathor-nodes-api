"""Calculate income gained from staking rewards."""
from datetime import date
from datetime import datetime

import pandas as pd
import requests

from flaskr.helper import load_config

class IncomeCalculator():
    """Class that collects methods for calculating staking & lp rewards."""
    def __init__(
        self,
        address: str,
        token: str,
        lcd: str,
        date_start: object,
        date_end: object
    ) -> None:
        self.address = address
        self.token = token
        self.lcd = lcd
        self.LP_TOKENS_URL = ('https://api-osmosis-chain.imperator.co/lp/v1/'
                                'rewards/token/{address}')
        self.LP_REWARDS_URL = ('https://api-osmosis-chain.imperator.co/lp/v1/'
                                'rewards/historical/{address}/{token}')
        self.TOKEN_PRICE_URL = ('https://api-osmosis.imperator.co/tokens/v1/'
                                'historical/{token}/chart?range=1y')

        self.date_start = date_start
        self.date_end = date_end
        if not self.date_start:
            self.date_start=date(date.today().year-1, 1, 1)
        if not self.date_end:
            self.date_end=date(date.today().year-1, 12, 31)

    def calc_staking_income(self) -> object:
        """Get all withdraw_rewards/commission"""
        denom = 'u' + self.token.lower()
        offset = 0
        limit = 100

        url = (f"{self.lcd}cosmos/tx/v1beta1/txs?events=message.sender="
               f"'{self.address}'&pagination.offset={offset}&"
               f"pagination.limit={limit}")
        print("URL HERE", flush=True)
        print(url, flush=True)
        res = requests.get(url).json()
        page_total = int(res['pagination']['total'])/limit
        tx_responses = res["tx_responses"]

        offset += limit
        while offset/limit <= int(page_total):
            url = (f"{self.lcd}cosmos/tx/v1beta1/txs?events=message.sender="
                   f"'{self.address}'&pagination.offset={offset}&"
                   f"pagination.limit={limit}")
            res = requests.get(url).json()
            tx_responses.extend(res["tx_responses"])
            offset += limit

        reward_tx = []
        for transaction in tx_responses:
            for msg in transaction['logs']:
                for event in msg['events']:
                    withdraw = event['type'].startswith('withdraw')
                    reward = event['type'].endswith('rewards')
                    commission = event['type'].endswith('commission')
                    if withdraw and (reward or commission):
                        amount = event['attributes'][0]['value']
                        if not amount:
                            amount = f'0{denom}'
                        rewards = amount.split(',')
                        for reward in rewards:
                            if denom in reward:
                                amount = int(reward.split(denom)[0])/1000000

                                day = datetime.strptime(
                                    transaction['timestamp'], '%Y-%m-%dT%H:%M:%SZ'
                                ).strftime('%Y-%m-%d')
                                reward_tx.append(
                                    {
                                        'day': day,
                                        'type': 'Staking',
                                        'token': self.token,
                                        'amount': amount
                                        }
                                    )

        if not reward_tx:
            reward_tx = [
                {'day': '1970-01-1', 'type': None, 'token': None, 'amount': 0}
            ]

        reward_tx = pd.DataFrame(reward_tx)
        reward_tx.set_index('day', inplace=True)
        reward_tx = reward_tx[['type', 'token', 'amount']]
        reward_tx['token'] = reward_tx['token'].str.upper()
        return reward_tx

    def _get_prices(self, token: str) -> object:
        """Get price of the given token"""
        price_url = self.TOKEN_PRICE_URL.format(token=token)
        prices = requests.get(price_url).json()
        for price in prices:
            time = price.pop('time')
            price['day'] = datetime.utcfromtimestamp(time).strftime('%Y-%m-%d')
            price['price'] = price['close']
        prices = [
            {'day': price['day'], 'token': token, 'price': price['price']}
            for price
            in prices
        ]
        prices = pd.DataFrame(prices)
        prices.set_index('day', inplace=True)
        return prices

    def _get_lp_rewards(self, token='OSMO') -> object:
        """Collect and price LP rewards for the user in the date range."""
        rewards_url = self.LP_REWARDS_URL.format(
            address=self.address, token=token
        )
        rewards = requests.get(rewards_url).json()
        rewards = pd.DataFrame(rewards)
        rewards.set_index('day', inplace=True)
        rewards['type'] = 'LP Reward'
        rewards['token'] = token
        return rewards

    def calc_lp_income(self) -> object:
        """Calculate total USD income for an address in Osmo Pool rewards."""

        # Identify which blockchain rewards are neccesaray.
        tokens_url = self.LP_TOKENS_URL.format(address=self.address)
        tokens = requests.get(tokens_url).json()
        tokens = [token['token'] for token in tokens]

        # Grab rewards transactions for each token.
        lp_rewards = [self._get_lp_rewards(token) for token in tokens]

        if not lp_rewards:
            lp_rewards = [
                {'day': '1970-01-1', 'type': None, 'token': None, 'amount': 0}
            ]
            lp_rewards = pd.DataFrame(lp_rewards)
            lp_rewards.set_index('day', inplace=True)
            lp_rewards = [lp_rewards, lp_rewards]

        lp_income = pd.concat(lp_rewards)
        lp_income = lp_income[['type', 'token', 'amount']]
        return lp_income

    def calc_taxable_income(self) -> object:
        """Combine LP & Staking Income into one dataframe."""
        lp_income = self.calc_lp_income()
        staking_income = self.calc_staking_income()
        tokens_url = self.LP_TOKENS_URL.format(address=self.address)
        tokens = requests.get(tokens_url).json()
        tokens = [token['token'] for token in tokens]
        if tokens:
            prices = pd.concat([self._get_prices(token) for token in tokens])
        else:
            prices = self._get_prices(self.token)
        all_income = pd.concat([lp_income, staking_income])
        all_income = all_income.merge(prices, on=['day', 'token'])
        all_income['income'] = all_income['price']*all_income['amount']

        # Filter by date requirements
        all_income.index = pd.to_datetime(all_income.index)
        all_income.index = all_income.index.date
        after_start = all_income.index >= self.date_start
        before_end = all_income.index <= self.date_end
        valid_date = after_start & before_end
        all_income = all_income.loc[valid_date]
        all_income.index.name = 'day'
        return all_income

    def get_taxable_income(self) -> str:
        """Calc taxable income based chain and return JSON string."""
        if self.token == 'osmo':
            taxable_income = self.calc_taxable_income()
        else:
            taxable_income = self.calc_staking_income()
            prices = self._get_prices(self.token)
            taxable_income = taxable_income.merge(prices, on=['day', 'token'])
            taxable_income['income'] = taxable_income.price*taxable_income.amount

            # Filter by date requirements
            taxable_income.index = pd.to_datetime(taxable_income.index)
            taxable_income.index = taxable_income.index.date
            after_start = taxable_income.index >= self.date_start
            before_end = taxable_income.index <= self.date_end
            valid_date = after_start & before_end
            taxable_income = taxable_income.loc[valid_date]
            taxable_income.index.name = 'day'
        taxable_income.reset_index(inplace=True)
        return taxable_income.to_json(orient='records')
