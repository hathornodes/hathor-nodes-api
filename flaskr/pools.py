import requests

def get_ibc_name_mapping():
    url = 'https://api-osmosis.imperator.co/search/v1/pools'
    response = requests.get(url)
    result = {}
    if response.status_code != 200:
        return

    data = response.json()

    for pool_id, pools in data.items():
        for pool in pools:
            result[pool['denom']] = {
                'symbol': pool['symbol'],
                'price': pool['price']
            }

    return result
